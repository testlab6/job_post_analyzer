#!/bin/bash
gcloud auth activate-service-account --key-file=/app/jobDataCollection/caramel-era-291218-ea4ee7899ef2.json
gcloud secrets list
gcloud secrets versions access 1 --secret=job_search > configurationSecrets.properties
mv configurationSecrets.properties /app/jobDataCollection
cd /app/jobDataCollection
mvn clean package
mvn -Dtest=IndeedPosts test
